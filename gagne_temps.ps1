# Partie 1: Chiffrement C
Write-Host "Chiffrement C"
cmd /c manage-bde -on C: -RecoveryPassword -UsedSpaceOnly -s 
cmd /c manage-bde -status
Write-Host "Chiffrement OK"

# Partie 2: Paramètres et actions supplémentaires
param (
    # Displays a help message
    [Alias("h")]
    [switch]$help,
    # Create a new user
    [switch]$c,
    # Update Windows 11
    [switch]$u
)

if ($help)
{
    # Aide
    Write-Host "Usage: " $MyInvocation.MyCommand.name "[-help] [-c] [-u]"
    Write-Host ""
    Write-Host "    -help   Affiche ce message"
    Write-Host "    -c      Créer un nouvel utilisateur"
    Write-Host "    -u      Mettre à jour Windows 11"
    Write-Host ""
    Write-Host "Remarque : si aucun argument n'est passé, ce script créera un nouvel utilisateur et mettra à jour Windows 11"
    exit
}

if ($c -or !$u)
{
    # Créer un nouvel utilisateur
    $prenom = Read-Host -Prompt "Entrez le prénom de l'utilisateur"
    $nom = Read-Host -Prompt "Entrez le nom de l'utilisateur"
    $pseudo = ($prenom[0].ToString().ToLower()) + ($nom.ToLower())
    $nomComplet = "$prenom $nom"
    $securePassword = Read-Host "Entrez le mot de passe" -AsSecureString
    New-LocalUser -Name $pseudo -FullName $nomComplet -Description "Cet utilisateur a été créé par le script PowerShell" -Password $securePassword
    Add-LocalGroupMember -Group "Administrateurs" -Member $pseudo
    # Oblige l'utilisateur à modifier le mot de passe lors de la prochaine ouverture de session
    Invoke-Expression "wmic UserAccount where name='$pseudo' set Passwordexpires=true"
    Invoke-Expression "net user ""$pseudo"" /logonpasswordchg:yes"
    Write-Host "L'utilisateur $nomComplet ($pseudo) a été créé avec succès, le mot de passe doit être changé lors de la prochaine ouverture de session"
}

if ($u -or !$c)
{
    # Mettre à jour Windows 11
    Install-Module PSWindowsUpdate -Force
    Set-ExecutionPolicy Unrestricted
    Install-WindowsUpdate -AcceptAll -AutoReboot
    Set-ExecutionPolicy Restricted
}

# Supprimez ce script une fois terminé
# Remove-Item -Path $MyInvocation.MyCommand.Path

